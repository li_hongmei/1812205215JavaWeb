<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2019/12/3
  Time: 11:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div class="header">
    <div class="container">
        <nav class="navbar navbar-default" role="navigation">
            <%--LOGO--%>
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <h1 class="navbar-brand"><a href="index">CAKE</a></h1>
            </div>
            <%--导航--%>
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="index" <c:if test="${param.flag==1}">class="active"</c:if>>首页</a></li>
                    <%--下拉列表--%>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle <c:if test="${param.flag==2}">active</c:if>" data-toggle="dropdown">商品分类<b class="caret"></b></a>
                        <ul class="dropdown-menu multi-column columns-2">
                            <li>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>商品分类</h4>
                                        <ul class="multi-column-dropdown">
                                            <li><a class="list" href="GoodsListServlet">全部系列</a></li>
                                            <c:forEach items="${Typelist}" var="t">
                                                <li><a class="list" href="GoodsListServlet?id=${t.id}">${t.name}</a></li>
                                            </c:forEach>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="/GoodsRecommendListServlet?type=2" <c:if test="${param.flag==3&&t==2}">class="active"</c:if>>热销</a></li>
                    <li><a href="/GoodsRecommendListServlet?type=3" <c:if test="${param.flag==3&&t==3}">class="active"</c:if>>新品</a></li>
                    <c:choose>
                        <c:when test="${empty user}">
                            <li><a href="/register.jsp?" <c:if test="${param.flag==4}">class="active"</c:if> >注册</a></li>
                            <li><a href="/login.jsp?"  <c:if test="${param.flag==5}">class="active"</c:if>>登录</a></li>
                        </c:when>
                        <c:otherwise>
                            <li><a href="/MyOrderServlet?"  <c:if test="${param.flag==6}">class="active"</c:if>>我的订单</a></li>
                            <li><a href="/usercenter.jsp?"  <c:if test="${param.flag==7}">class="active"</c:if>>个人中心</a></li>
                            <li><a href="/UserLogoutServlet" >退出</a></li>
                        </c:otherwise>
                    </c:choose>
                    <c:if test="${!empty user&&user.isadmin}">
                        <li><a href="/admin/admin_index.jsp" target="_blank">后台管理</a></li>
                    </c:if>
                </ul>
            </div>
        </nav>

        <div class="header-info">
            <%--查询功能--%>
            <div class="header-right search-box">
                <a href="javascript:;"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                <div class="search">
                    <form class="navbar-form" action="GoodsSearchServlet">
                        <input type="text" class="form-control" name="keyword" />
                        <button type="submit" class="btn btn-default" aria-label="Left Align">搜索</button>
                    </form>
                </div>
            </div>
            <%--购物车--%>
            <div class="header-right cart">
                <a href="cart.jsp">
                    <span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"><span class="card_num">${order.amount}</span></span>
                </a>
            </div>
            <%--管理员登出--%>
            <div class="header-right login">
                <a href="my.action"><span class="glyphicon" aria-hidden="true"></span></a>
            </div>
            <%--清除浮动--%>
            <div class="clearfix"> </div>
        </div>
        <%--清除浮动--%>
        <div class="clearfix"> </div>

    </div>
</div>
